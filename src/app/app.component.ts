import { Component } from '@angular/core';

@Component({
  /*standalone: true, */
  selector: 'app-root',/*nombre del selector que se utilizará para identificar el componente en otros archivos HTML */
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Tour of Heroes';
}
